import pytest

from src import OHCE, Langue
from test.utils import HorlogeBuilder

CAS_TRAITES_BONJOUR = [
    ("Langue EN tres tot ", {"langue": "en", "heure": 0, "message": Langue('en').messages_bonjour[1]}),
    ("Langue EN tres tot ", {"langue": "en", "heure": 2, "message": Langue('en').messages_bonjour[1]}),
    ("Langue EN tres tot ", {"langue": "en", "heure": 4, "message": Langue('en').messages_bonjour[1]}),
    ("Langue EN le matin ", {"langue": "en", "heure": 6, "message": Langue('en').messages_bonjour[2]}),
    ("Langue EN le matin ", {"langue": "en", "heure": 8, "message": Langue('en').messages_bonjour[2]}),
    ("Langue EN le matin ", {"langue": "en", "heure": 10, "message": Langue('en').messages_bonjour[2]}),
    ("Langue EN l'apres midi ", {"langue": "en", "heure": 12, "message": Langue('en').messages_bonjour[3]}),
    ("Langue EN l'apres midi ", {"langue": "en", "heure": 14, "message": Langue('en').messages_bonjour[3]}),
    ("Langue EN l'apres midi ", {"langue": "en", "heure": 16, "message": Langue('en').messages_bonjour[3]}),
    ("Langue EN le soir ", {"langue": "en", "heure": 18, "message": Langue('en').messages_bonjour[4]}),
    ("Langue EN le soir ", {"langue": "en", "heure": 20, "message": Langue('en').messages_bonjour[4]}),
    ("Langue EN le soir ", {"langue": "en", "heure": 22, "message": Langue('en').messages_bonjour[4]}),
    ("Langue EN le matin ", {"langue": "en", "heure": 24, "message": Langue('en').messages_bonjour[1]}),
    ("Langue FR tres tot ", {"langue": "fr", "heure": 0, "message": Langue('fr').messages_bonjour[1]}),
    ("Langue FR tres tot ", {"langue": "fr", "heure": 2, "message": Langue('fr').messages_bonjour[1]}),
    ("Langue FR tres tot ", {"langue": "fr", "heure": 4, "message": Langue('fr').messages_bonjour[1]}),
    ("Langue FR le matin ", {"langue": "fr", "heure": 6, "message": Langue('fr').messages_bonjour[2]}),
    ("Langue FR le matin ", {"langue": "fr", "heure": 8, "message": Langue('fr').messages_bonjour[2]}),
    ("Langue FR le matin ", {"langue": "fr", "heure": 10, "message": Langue('fr').messages_bonjour[2]}),
    ("Langue FR l'apres midi ", {"langue": "fr", "heure": 12, "message": Langue('fr').messages_bonjour[3]}),
    ("Langue FR l'apres midi ", {"langue": "fr", "heure": 14, "message": Langue('fr').messages_bonjour[3]}),
    ("Langue FR l'apres midi ", {"langue": "fr", "heure": 16, "message": Langue('fr').messages_bonjour[3]}),
    ("Langue FR le soir ", {"langue": "fr", "heure": 18, "message": Langue('fr').messages_bonjour[4]}),
    ("Langue FR le soir ", {"langue": "fr", "heure": 20, "message": Langue('fr').messages_bonjour[4]}),
    ("Langue FR le soir ", {"langue": "fr", "heure": 22, "message": Langue('fr').messages_bonjour[4]}),
    ("Langue FR le matin ", {"langue": "fr", "heure": 24, "message": Langue('fr').messages_bonjour[1]}),
]


@pytest.mark.parametrize(
    argnames=('name', 'contexte'), argvalues=CAS_TRAITES_BONJOUR,
    ids=[cas[0] for cas in CAS_TRAITES_BONJOUR]
)
def test_ohce_heure_bonjour_en_langue(name, contexte):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    ET que la période de la journée est < période >
    QUAND on saisit une chaîne
    ALORS < salutation > de cette langue à cette période est envoyé avant tout.
    """
    # ÉTANT DONNE un utilisateur parlant une langue
    # ET que la période de la journée est < période >
    langue, heure = contexte['langue'], contexte['heure']
    ohce = OHCE(langue=Langue(langue), horloge=HorlogeBuilder().prends_comme_heure(heure))
    # QUAND on saisit une chaine.
    chaine = str()
    miroir = ohce.palindrome(chaine)
    #  ALORS < salutation > de cette langue à cette période est envoyé avant tout.
    salutations_attendues = contexte['message']
    assert miroir.startswith(salutations_attendues), f'{miroir} ne commence pas par {salutations_attendues}'


CAS_TRAITES_AU_REVOIR = [
    "Cas tot le matin en langue ",
    "Cas tot le matin en langue ",
    "Cas tot le matin en langue ",
    "Cas la matinee en langue ",
    "Cas la matinee en langue ",
    "Cas l'apres midi en langue ",
    "Cas le soir en langue ",
    "Cas le soir en langue "
]


@pytest.mark.parametrize("_langue", ['fr', 'en'])
@pytest.mark.parametrize("_heure, index_message_attendu",
                         [(0, 1), (2, 1), (3, 1), (6, 2),
                          (8, 2), (14, 3), (20, 4), (22, 4)],
                         ids=[cas for cas in CAS_TRAITES_AU_REVOIR])
def test_ohce_heure_au_revoir_en_langue(_langue, _heure, index_message_attendu):
    """
    ETANT DONNE un utilisateur parlant une langue
    ET que la période de la journée est < période >
    QUAND on saisit une chaîne
    ALORS < auRevoir > dans cette langue à cette période est envoyé en dernier
    """
    # ETANT DONNE un utilisateur parlant une langue
    # ET que la période de la journée est < période >
    langue_actuelle = Langue(_langue)
    horloge_actuelle = HorlogeBuilder().prends_comme_heure(_heure)
    ohce = OHCE(langue=langue_actuelle, horloge=horloge_actuelle)
    # QUAND on saisit une chaine.
    chaine = str()
    miroir = ohce.palindrome(chaine)
    #  ALORS < auRevoir > dans cette langue à cette période est envoyé en dernier.
    au_revoir_attendu = langue_actuelle.messages_au_revoir[index_message_attendu]
    assert miroir.endswith(au_revoir_attendu + '\n'), f'{miroir} ne finit pas par {au_revoir_attendu}'


