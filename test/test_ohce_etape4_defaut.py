import re

import pytest

from src import OHCE, Langue
from test.utils import HorlogeBuilder


@pytest.mark.parametrize("_langue", ['fr', 'en'])
@pytest.mark.parametrize("_heure", [_ for _ in range(0, 24, 4)])
def test_defaut_saut_de_ligne_palindrome(_langue, _heure):
    """
    ETANT DONNE un utilisateur parlant une langue
    ET que la période de la journée est < période >
    QUAND on saisit une chaîne
    ALORS < auRevoir > dans cette langue à cette période est envoyé en dernier
    """
    # ETANT DONNE un utilisateur parlant une langue
    # ET que la période de la journée est < période >
    langue_actuelle = Langue(_langue)
    horloge_actuelle = HorlogeBuilder().prends_comme_heure(_heure)
    ohce = OHCE(langue=langue_actuelle, horloge=horloge_actuelle)
    # QUAND on saisit un palindrome.
    chaine = 'radar'
    miroir = ohce.palindrome(chaine)
    #  ALORS la réponse comporte 4 sauts de ligne
    assert len([m.start() for m in re.finditer('\n', miroir)]) == 4


@pytest.mark.parametrize("_langue", ['fr', 'en'])
@pytest.mark.parametrize("_heure", [_ for _ in range(0, 24, 4)])
def test_defaut_saut_de_ligne_chaine_normale(_langue, _heure):
    """
    ETANT DONNE un utilisateur parlant une langue
    ET que la période de la journée est < période >
    QUAND on saisit une chaîne
    ALORS < auRevoir > dans cette langue à cette période est envoyé en dernier
    """
    # ETANT DONNE un utilisateur parlant une langue
    # ET que la période de la journée est < période >
    langue_actuelle = Langue(_langue)
    horloge_actuelle = HorlogeBuilder().prends_comme_heure(_heure)
    ohce = OHCE(langue=langue_actuelle, horloge=horloge_actuelle)
    # QUAND on saisit une chaine.
    chaine = 'gibson'
    miroir = ohce.palindrome(chaine)
    #  ALORS la réponse comporte 3 sauts de ligne
    assert len([m.start() for m in re.finditer('\n', miroir)]) == 3




