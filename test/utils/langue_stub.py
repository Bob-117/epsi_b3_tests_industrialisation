from src import Langue, OHCE


class LangueStub:

    def __init__(self):
        self.__default = str()

    @property
    def message_bien_dit(self):
        return self.__default

    @property
    def messages_bonjour(self):
        return {}

    def message_bonjour(self, _horloge=None):
        return self.__default

    @property
    def messages_au_revoir(self):
        return {}

    def message_au_revoir(self, _horloge=None):
        return self.__default


if __name__ == '__main__':
    L = ['fr', 'en']
    for l in L:
        _l = Langue(l)
        b = OHCE(_l)
        print(b.palindrome('aha'))
    c = OHCE(LangueStub())
    print(c.palindrome('aha'))
