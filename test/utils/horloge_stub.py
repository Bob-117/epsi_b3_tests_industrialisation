class HorlogeStub:

    def __init__(self):
        self.__heure = 1
        self.__heures_par_jour = 24

    @property
    def heure(self):
        return self.__heure

    @property
    def heures_par_jour(self):
        return self.__heures_par_jour
