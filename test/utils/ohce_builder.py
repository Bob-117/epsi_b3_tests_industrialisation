from src import OHCE
from test.utils import HorlogeStub
from test.utils.langue_stub import LangueStub


class OHCEBuilder:
    debug = 'xyz'
    _langue = LangueStub()

    def build(self):
        return OHCE(langue=self._langue, horloge=HorlogeStub())

    def prends_comme_langue(self, lang):
        """
        # OHCEBuilder().prends_comme_langue(Francais).build()
        """
        self._langue = lang
        return self

    def gibson(self):
        print(self.debug)


if __name__ == '__main__':
    a = OHCEBuilder()
    a.gibson()
    o = a.build()
    a.gibson()
    print(o.bien_dit())
