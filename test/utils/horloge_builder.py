from src import Horloge


class HorlogeBuilder:

    __heure = 0
    __heures_par_jour = 24

    def build(self):
        return Horloge()

    def prends_comme_heure(self, heure):
        """
        # OHCEBuilder().prends_comme_heure(8).build()
        """

        self.__heure = abs(heure) % self.heures_par_jour
        return self

    @property
    def heure(self):
        return self.__heure

    @property
    def heures_par_jour(self):
        return self.__heures_par_jour

