import pytest

from src import OHCE, Langue
from test.utils import HorlogeStub


@pytest.mark.parametrize("_langue", ['fr', 'en'])
def test_ohce_langue_bien_dit(_langue):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    QUAND on entre un palindrome
    ALORS il est renvoyé
    ET le < bienDit > de cette langue est envoyé.
    :return:
    """
    # ÉTANT DONNE OHCE CONFIGURÉ DANS UNE LANGUE
    langue_selectionnee = Langue(_langue)
    ohce = OHCE(langue=langue_selectionnee, horloge=HorlogeStub())
    # QUAND on entre un palindrome.
    miroir = ohce.palindrome('radar')
    # ALORS, il est renvoyé ET le < bienDit > de cette langue est envoyé.
    assert ohce.bien_dit() == langue_selectionnee.message_bien_dit
    assert miroir.__contains__(langue_selectionnee.message_bien_dit), f'{miroir} ne contient pas <Biendit> en {_langue}'
    assert miroir.__contains__('radar'), f'miroir {miroir} ne contient pas "radar"'
    assert miroir.find(langue_selectionnee.message_bien_dit) > miroir.find('radar'), 'bien dit avant palindrome, pas apres'


@pytest.mark.parametrize("_langue", ['fr', 'en'])
def test_ohce_langue_bonjour_avant_tout(_langue):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    QUAND on saisit une chaîne
    ALORS < bonjour > de cette langue est envoyé avant tout.
    :return:
    """
    # ÉTANT DONNE OHCE CONFIGURÉ DANS UNE LANGUE
    langue_selectionnee = Langue(_langue)
    ohce = OHCE(langue=langue_selectionnee, horloge=HorlogeStub())
    # QUAND on saisit une chaine.
    chaine = str()
    miroir = ohce.palindrome(chaine)
    # ALORS < bonjour > de cette langue est envoyé avant tout.
    assert miroir.startswith(langue_selectionnee.message_bonjour(HorlogeStub())), 'miroir ne commence pas par <Bonjour>'


@pytest.mark.parametrize("_langue", ['fr', 'en'])
def test_ohce_lange_au_revoir_apres_tout(_langue):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    QUAND on saisit une chaîne
    ALORS < auRevoir > dans cette langue est envoyé en dernier.
    :return:
    """
    # ÉTANT DONNE OHCE CONFIGURÉ DANS UNE LANGUE
    langue_selectionnee = Langue(_langue)
    ohce = OHCE(langue=langue_selectionnee, horloge=HorlogeStub())
    # QUAND on saisit une chaine.
    chaine = str()
    miroir = ohce.palindrome(chaine)
    # ALORS < auRevoir > de cette langue est envoyé en dernier.
    assert miroir.endswith(langue_selectionnee.message_au_revoir(HorlogeStub()) + '\n'), 'miroir ne finit pas par <AuRevoir>'


