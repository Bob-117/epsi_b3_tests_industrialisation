from src import OHCE
from test.utils import LangueStub, HorlogeStub


# Étape 1
# - QUAND on saisit une chaîne ALORS celle-ci est renvoyée en miroir.
# - QUAND on saisit un palindrome ALORS celui-ci est renvoyé ET « Bien dit » est envoyé ensuite.
# - QUAND on saisit une chaîne ALORS « Bonjour » est envoyé avant toute réponse.
# - QUAND on saisit une chaîne ALORS « Au revoir » est envoyé en dernier.

def test_ohce_palindrome_miroir():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit une chaîne
    ALORS celle-ci est renvoyée en miroir.
    """
    # ÉTANT DONNE OHCE
    _ohce = OHCE(LangueStub(), HorlogeStub())
    # QUAND on saisit une chaîne.
    _chaine = 'python'
    _miroir = _ohce.palindrome(_chaine)
    # ALORS celle-ci est renvoyée en miroir.
    assert _miroir.__contains__('nohtyp'), 'La reponse doit contenir la chaine en miroir'


def test_ohce_palindrome_bien_dit():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit un palindrome
    ALORS celui-ci est renvoyé
    ET « Bien dit » est envoyé ensuite.
    """
    # ÉTANT DONNE OHCE
    _ohce = OHCE(LangueStub(), HorlogeStub())
    # QUAND on saisit un palindrome.
    _chaine = 'radar'
    _miroir = _ohce.palindrome(_chaine)
    # ALORS celui-ci est renvoyé ET « Bien dit » est envoyé ensuite.
    assert _miroir.__contains__('radar'), 'La reponse doit contenir la chaine en miroir'
    assert _miroir.__contains__(LangueStub().message_bien_dit), 'La reponse doit contenir < Bien dit >'


def test_ohce_palindrome_bonjour_avant_tout():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit une chaîne
    ALORS « Bonjour » est envoyé avant toute réponse.
    """
    # ÉTANT DONNE OHCE
    _ohce = OHCE(LangueStub(), HorlogeStub())
    # QUAND on saisit une chaîne.
    _chaine = str()
    _miroir = _ohce.palindrome(_chaine)
    # ALORS « Bonjour » est envoyé avant toute réponse.
    assert _miroir.startswith(LangueStub().message_bonjour()), 'La réponse doit commencer par < Bonjour> '


def test_ohce_palindrome_au_revoir_apres_tout():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit une chaîne
    ALORS « Au revoir » est envoyé en dernier.
    :return:
    """
    # ÉTANT DONNE OHCE
    _ohce = OHCE(LangueStub(), HorlogeStub())
    # QUAND on saisit une chaîne.
    _chaine = str()
    _miroir = _ohce.palindrome(_chaine)
    # ALORS « Au revoir » est envoyé en dernier.
    assert _miroir.endswith(LangueStub().message_au_revoir(HorlogeStub().heure)), 'La réponse doit finir par <AuRevoir>'
