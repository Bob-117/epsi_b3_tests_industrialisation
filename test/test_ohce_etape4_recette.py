import builtins
from unittest import mock

from src import OhceUiCli, Langue
from test.utils import HorlogeBuilder


def test_recette_auto_palindrome_anglais_soir(capsys):
    # ÉTANT DONNE UNE APPLICATION OHCE AVEC UNE INTERFACE CLI
    app = OhceUiCli()
    # ET LE MODE AUTOMATIQUE AVEC UN PALINDROME PAR DEFAUT
    palindrome_par_defaut = app.palindrome_defaut
    # ET UNE LANGUE ANGLAISE
    langue = Langue('en')
    # ET vingt-deux heure 22h00
    horloge = HorlogeBuilder().prends_comme_heure(22)
    # QUAND ON JOUE UNE PARTIE
    app.jouer_une_partie(
        auto=True, _langue=langue, _horloge=horloge, palindrome=True
    )
    # ALORS L'APPLICATION REPOND CORRECTEMENT
    reponse = capsys.readouterr()
    assert reponse.out == f'Good night\n{palindrome_par_defaut[::-1]}\nWell said\nHave a good night !\n'


def test_recette_auto_non_palindrome_francais_matin(capsys):
    # ÉTANT DONNE UNE APPLICATION OHCE AVEC UNE INTERFACE CLI
    app = OhceUiCli()
    # ET LE MODE AUTOMATIQUE AVEC UNE CHAINE NON PALINDROME PAR DEFAUT
    chaine_par_defaut = app.chaine_defaut
    # ET UNE LANGUE FRANCAISE
    langue = Langue('fr')
    # ET vingt-deux heure 22h00
    horloge = HorlogeBuilder().prends_comme_heure(5)
    # QUAND ON JOUE UNE PARTIE
    app.jouer_une_partie(
        auto=True, _langue=langue, _horloge=horloge, palindrome=False
    )
    # ALORS L'APPLICATION REPOND CORRECTEMENT
    reponse = capsys.readouterr()
    assert reponse.out == f'Il est tot !\n{chaine_par_defaut[::-1]}\nBonne nuit !\n'


def test_recette_saisie_libre_langue_heure_systeme(capsys):
    # ÉTANT DONNE UNE APPLICATION OHCE AVEC UNE INTERFACE CLI
    app = OhceUiCli()
    # ET LE MODE SAISIE LIBRE
    mode_automatique = False
    mot_saisi_par_le_client = 'client'
    miroir_du_client = mot_saisi_par_le_client[::-1]

    # QUAND ON JOUE UNE PARTIE AVEC LANGUE ET HEURE DU SYSTEME
    with mock.patch.object(builtins, 'input', lambda _: mot_saisi_par_le_client):
        app.jouer_une_partie(
            auto=mode_automatique
        )
        # ALORS L'APPLICATION REPOND CORRECTEMENT
        reponse = capsys.readouterr()
        assert reponse.out.__contains__(miroir_du_client), 'Le miroir du client n\'apparait pas dans la reponse !'
