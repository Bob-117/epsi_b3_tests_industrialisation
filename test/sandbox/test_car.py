import pytest

from test.sandbox import Park, CarStub, ParkSpy, Car, CustomerBuilder, Customer


def test_empty_park_raise_exception():
    # Arrange
    park = Park()
    # Act
    message = park.messages[3]
    # Assert
    with pytest.raises(Exception) as info:
        park.pick_a_car()
    assert str(info.value) == message


@pytest.mark.xfail(reason='park is empty')
def test_empty_park_will_fail_mark():
    # Arrange
    park = Park()
    # Act
    car = park.pick_a_car()
    # Assert
    assert car


@pytest.mark.skip(reason="Working on it")
def test_will_be_skipped():
    assert False


def test_park_with_stub_cars():
    # Arrange
    park = Park()
    car1 = car2 = CarStub()
    park.add_car(car1)
    park.add_car(car2)
    # Act
    car = park.pick_a_car()
    # Assert
    assert len(park.cars) == 2
    assert car in [car1, car2]


def test_park_spy():
    # Arrange
    park_spy = ParkSpy()
    car1, car2, car3, car4 = Car('RED'), Car('BLUE'), Car('RED'), Car('GREEN')
    park_spy.add_car(car1)
    park_spy.add_car(car2)
    park_spy.add_car(car3)
    park_spy.add_car(car4)
    # Act
    before = park_spy.red_car_sell
    park_spy.remove_a_car(car1)
    progress = park_spy.red_car_sell
    park_spy.remove_a_car(car2)
    park_spy.remove_a_car(car3)
    park_spy.remove_a_car(car4)
    after = park_spy.red_car_sell
    # Assert
    assert before == 0
    assert progress == 1
    assert after == 2


@pytest.mark.parametrize("age, money, message",
                         [(19, 0, 'You are too young !'), (19, 10e10, 'You are too young !'),
                          (21, 0, 'Not enough money !'), (21, 10e10, 'Deal !')])
def test_customer_builder(age, money, message):
    # Arrange
    park = Park()
    car = Car('GREEN')
    park.add_car(car)

    customer = CustomerBuilder().with_age(age).with_money(money).build()
    r = park.sell_a_car(customer)
    assert r == message


@pytest.mark.parametrize('age, money', [(19, 0), (19, 10e10), (22, 0), (22, 10e10)])
@pytest.mark.parametrize('car_color', ['RED', 'GREEN', 'BLUE'])
@pytest.mark.parametrize('messages', [['You are too young !', 'Not enough money !', 'Deal !']])
def test_parametrize(age, money, car_color, messages):
    # Arrange
    _ = f'param1 : {age} - param2 : {money} - param3 : {car_color} - param4 : {messages}'
    park = Park()
    car = Car(color=car_color)
    park.add_car(car)
    customer = Customer(age=age, money=money)
    # Act
    r = park.sell_a_car(customer)
    expected_message = messages[
        2 if (customer.age > 20 and customer.money > car.calculate_price()) else (1 if customer.age > 20 else 0)
    ]
    # Assert
    assert r == expected_message


TEST_CASES = [
    ("Too young", {'age': 19, 'money': 0, 'car_color': 'RED', 'message': 'You are too young !'}),
    ("Enough money but too young", {'age': 19, 'money': 10e10, 'car_color': 'RED', 'message': 'You are too young !'}),
    ("Not enough money", {'age': 22, 'money': 0, 'car_color': 'RED', 'message': 'Not enough money !'}),
    ("Deal !", {'age': 22, 'money': 10e10, 'car_color': 'RED', 'message': 'Deal !'}),
]


@pytest.mark.parametrize(
    argnames=('_', 'params'), argvalues=TEST_CASES,
    ids=[cas[0] for cas in TEST_CASES]
)
def test_parametrize_with_custom_name(_, params):
    # Arrange
    park = Park()
    car = Car(color=params['car_color'])
    park.add_car(car)
    customer = Customer(age=params['age'], money=params['money'])
    # Act
    r = park.sell_a_car(customer)
    expected_message = params['message']
    # Assert
    assert r == expected_message


def test_generator():
    """
    AVG price on X sales ?
    """
