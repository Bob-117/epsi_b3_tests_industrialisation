import os
from datetime import datetime

import pytest


# ## SAVE TEST RESULTS IN FILE & CUSTOM TEST NAMES## #
def save_result_in_file(test, msg, first=False):
    _now = datetime.now().strftime("%d/%m/%Y-%H:%M:%S")
    _current = os.environ["PYTEST_CURRENT_TEST"][:-6]  # test name
    _log = _current + ("Fail" if not test else "") if not first else f'{"_" * 15}{_now}{"_" * 15}'
    # TODO pytest rootdir
    with open(f"{os.path.dirname(os.path.abspath(__file__))}/reports/report.log", "a+") as f:
        f.write(f'{_log}\n')
    assert test, msg


def test_init_date_in_log_file():
    save_result_in_file(True, 'first', first=True)


data = [
    ('test_square_two', [2, 4]),
    ('test_square_three', [3, 9]),
    ('test_square_four', [4, 17]),
    ('test_square_five', [5, 25])
]


@pytest.mark.parametrize(
    argnames=('test_name', 'number_and_square_couple'), argvalues=data,
    ids=[i[0] for i in data]
)
def test_square_number(test_name, number_and_square_couple):
    save_result_in_file(
        test=number_and_square_couple[0] ** 2 == number_and_square_couple[1],
        msg=f'Fail : {test_name} : {number_and_square_couple[0] ** 2} != {number_and_square_couple[1]}'
    )


# ## MARK ## #

@pytest.mark.skip(reason="Working on it")
def test_will_be_skipped():
    assert False


@pytest.mark.xfail(reason='nope')
def test_will_fail_anyway():
    assert False


# ## DOC ## #
# https://stackoverflow.com/questions/40880259/how-to-pass-arguments-in-pytest-by-command-line
# https://stackoverflow.com/questions/34466027/in-pytest-what-is-the-use-of-conftest-py-files
# https://www.ontestautomation.com/pytest-and-custom-command-line-arguments/
