import random
from enum import Enum


class Prices(Enum):
    RED = 1_000_000
    BLUE = 117
    GREEN = 3.14159265


class Car:
    def __init__(self, color):
        self.color = color

    def calculate_price(self):
        return Prices[self.color].value

    def __str__(self):
        return self.color

    def __repr__(self):
        return self.color


class CarStub:
    def __init__(self):
        self.price = 0

    def calculate_price(self):
        return self.calculate_price()


class Park:
    def __init__(self):
        self.cars = []
        self.messages = ['You are too young !', 'Not enough money !', 'Deal !', 'No car to sell !']

    def add_car(self, car):
        self.cars.append(car)

    def pick_a_car(self):
        if len(self.cars) <= 0:
            raise Exception(self.messages[3])

        _car = random.choice(self.cars)

        # index = random.randrange(len(self.cars))
        # _car = self.cars[index]

        return _car

    def remove_a_car(self, car):
        self.cars.remove(car)

    @staticmethod
    def confirm_a_sale(user, car):
        money_condition = user.money > car.calculate_price()
        age_condition = user.age > 20
        return age_condition, money_condition

    def sell_a_car(self, user):

        _car = self.pick_a_car()

        age_condition, money_condition = Park.confirm_a_sale(user, _car)

        if age_condition and money_condition:
            self.remove_a_car(_car)
            user.buy_a_car(_car)
            return self.response(2)
        else:
            return self.response(0) if not age_condition else self.response(1)

    def response(self, x):
        return self.messages[x]

    def total_price(self):
        return sum(car.calculate_price() for car in self.cars)


class ParkSpy(Park):
    def __init__(self):
        super().__init__()
        self.red_car_sell = 0

    def sell_a_car(self, user):
        super().sell_a_car(user)

    def remove_a_car(self, car):
        if car.color == 'RED':
            self.red_car_sell += 1
        super().remove_a_car(car)


# def enum(**enums):
#     return type('Enum', (), enums)
#
# Prices = enum(RED=1, BLUE=2, GREEN=0)


class Customer:
    def __init__(self, age, money):
        self.age = age
        self.money = money
        self.car = None

    def buy_a_car(self, car):
        self.money -= car.calculate_price()
        self.car = car


class CustomerBuilder:
    age = 0
    money = 0

    def build(self):
        return Customer(age=self.age, money=self.money)

    def with_age(self, age):
        self.age = age
        return self

    def with_money(self, money):
        self.money = money
        return self


def one_sale(_park, _c):
    print(_park.sell_a_car(_c))


if __name__ == '__main__':
    car1, car2 = Car('RED'), Car('BLUE')
    park = ParkSpy()
    park.add_car(car1)
    park.remove_a_car(car1)
    park.add_car(car2)
    park.add_car(car1)
    c = Customer(21, 10e50)
    one_sale(park, c)
    one_sale(park, c)
    one_sale(park, c)
