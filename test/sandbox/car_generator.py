from typing import Iterator

from test.sandbox import Car


class ColorBuilder:
    default = 'BLUE'

    def build(self):
        return self.default


class CarBuilder:
    def __init__(self):
        self.color = ColorBuilder().build()

    def with_color(self, color):
        self.color = color
        return self

    def build(self) -> Car:
        return Car(self.color)


class CarGenerator:
    def __init__(self, preconfigured=None):
        self._prototype_builder = preconfigured or CarBuilder()

    def generate_one_type(self, how_many: int) -> Iterator[Car]:
        for i in range(how_many):
            yield self._prototype_builder.build()

    def generate_various_color(self, table):
        for color, how_many in table:
            for i in range(how_many):
                yield self._prototype_builder.with_color(color).build()

    def with_color(self, peinture):
        return CarGenerator(self._prototype_builder.with_color(peinture))


if __name__ == '__main__':
    car_generator1 = CarGenerator()
    cars1 = car_generator1.generate_one_type(3)
    print(list(cars1))

    car_generator2 = CarGenerator()
    cars2 = car_generator2.with_color('RED').generate_one_type(8)
    print(list(cars2))

    cars3 = car_generator2.generate_various_color([('RED', 3), ('GREEN', 2)])
    print(list(cars3))
