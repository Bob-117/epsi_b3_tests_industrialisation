import locale

dictionnaire = {
    'fr': {
        'bonjour': {
            1: 'Il est tot !',
            2: 'Bonjour !',
            3: 'Bonne journée ?',
            4: 'Encore debout ?'
        },
        'auRevoir': {
            1: 'Bonne nuit !',
            2: 'Bonne journée !',
            3: 'Bon apres midi !',
            4: 'Bonne soiree !'
        },
        'bienDit': 'Bien dit',
    },
    'en': {
        'bonjour': {
            1: 'Early awake ? !',
            2: 'Hello !',
            3: 'Good evening',
            4: 'Good night'
        },
        'auRevoir': {
            1: '. _.',
            2: 'Have a good day !',
            3: 'Have a good evening !',
            4: 'Have a good night !'
        },
        'bienDit': 'Well said',
    }
}


class Langue:

    def __init__(self, _langue_selectionnee=None):
        self.__langue_selectionnee = _langue_selectionnee or locale.getdefaultlocale()[0][0:2]
        self.__messages = self.charger_le_dictionnaire(self.__langue_selectionnee)

    @staticmethod
    def charger_le_dictionnaire(_langue_selectionnee):
        """
        Charge le dictionnaire d'une langue choisie.
        :  _langue_selectionnee :
        """
        return dictionnaire.get(_langue_selectionnee)

    @property
    def message_bien_dit(self):
        """
        Message < BienDit > dans la langue selectionne.
        :return:
        """
        return self.__messages.get('bienDit')

    @property
    def messages_bonjour(self):
        """
        Dictionnaire des messages < Bonjour >
        :return:
        """
        return self.__messages.get('bonjour')

    def message_bonjour(self, _horloge):
        """
        Message < Bonjour > dans la langue selectionne a heure donnee.
        :return:
        """
        index_message = self.index_message_par_heure(_horloge, self.messages_bonjour)
        return self.messages_bonjour.__getitem__(index_message)
        # return self.messages_bonjour.__getitem__((_heure.heure // (24 / len(self.messages_bonjour))) + 1)
        # return self.messages_bonjour.__getitem__(self.index_message_par_heure(_heure, self.messages_bonjour))

    @property
    def messages_au_revoir(self):
        """
        Dictionnaire des messages < AuRevoir >.
        :return:
        """
        return self.__messages.get('auRevoir')

    def message_au_revoir(self, _horloge):
        """
        Message < AuRevoir > dans la langue selectionnee.
        :return:
        """
        index_message = self.index_message_par_heure(_horloge, self.messages_au_revoir)
        return self.messages_au_revoir.__getitem__(index_message)

    @staticmethod
    def index_message_par_heure(horloge, dictionnaire_messages):
        """
        Donne l'index d'un message dans un dictionnaire de messages en fonction de l'heure
        """
        return (horloge.heure // (horloge.heures_par_jour / len(dictionnaire_messages))) + 1
