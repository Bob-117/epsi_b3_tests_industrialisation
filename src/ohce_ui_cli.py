from src import OHCE, Langue, Horloge


class OhceUiCli:
    """
    Interface utilisateur en ligne de commande pour l'application OHCE
    """

    # TODO clean lang dict process (move to assets)
    available = ['fr', 'en']
    palindrome_defaut = 'radar'
    chaine_defaut = 'gibson'

    @staticmethod
    def langue_et_heure_de_la_partie(_langue=None, _heure=None):
        """
        Configuration de la langue et de l'heure.
        :param _langue:
        :param _heure:
        :return:
        """
        langue_de_la_partie = Langue(_langue)
        heure_de_la_partie = Horloge()
        if _heure:
            heure_de_la_partie.heure(_heure)
        return langue_de_la_partie, heure_de_la_partie

    @staticmethod
    def afficher_la_reponse(reponse):
        print(reponse, end='')

    @staticmethod
    def demander_un_mot():
        return input('mot ?')

    @staticmethod
    def jouer_une_fois(ohce, mot=None):
        if not mot:
            mot = OhceUiCli.demander_un_mot()
        reponse = ohce.palindrome(mot)
        OhceUiCli.afficher_la_reponse(reponse)
        return mot

    @staticmethod
    def jouer_en_boucle(_ohce):
        r = OhceUiCli.jouer_une_fois(_ohce)
        while r not in ['quit', 'quitter']:
            r = OhceUiCli.jouer_une_fois(_ohce)

    def jouer_une_partie(self, auto=True, _langue=None, _horloge=None, palindrome=False, repeat=False):

        if not _langue or not _horloge:
            heure_et_langue = OhceUiCli.langue_et_heure_de_la_partie()
            ohce = OHCE(*heure_et_langue)
        else:
            ohce = OHCE(langue=_langue, horloge=_horloge)

        if auto:
            _mot = self.palindrome_defaut if palindrome else self.chaine_defaut
            OhceUiCli.jouer_une_fois(ohce=ohce, mot=_mot)
        else:
            OhceUiCli.jouer_en_boucle(_ohce=ohce) if repeat else OhceUiCli.jouer_une_fois(ohce=ohce)


if __name__ == '__main__':
    app = OhceUiCli()
    app.jouer_une_partie(
        auto=True, _langue=Langue(), _horloge=Horloge(), palindrome=False, repeat=False
    )
