from datetime import datetime


class Horloge:
    """
    Classe Horloge qui donne l'heure H actuelle
    """
    def __init__(self, heures_par_jour=24):
        self.__heure = datetime.now().hour
        self.__heures_par_jour = heures_par_jour

    @property
    def heure(self):
        return self.__heure

    @heure.setter
    def heure(self, nouvelle_heure):
        self.__heure = abs(nouvelle_heure % self.__heures_par_jour)

    @property
    def heures_par_jour(self):
        return self.__heures_par_jour

    def __str__(self):
        return f'clock:{self.heure}'


