def saut_de_ligne(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs) + '\n'

    return wrapper
