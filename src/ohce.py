"""
Module OHCE
"""
from src.utils import saut_de_ligne


class OHCE:
    """
    Classe OHCE d'exercice
    """

    def __init__(self, langue, horloge):
        self.__langue = langue
        self.__horloge = horloge

    def palindrome(self, mot: str) -> str:
        """
        Methode principale de la class OHCE
        :param mot:
        """
        return OHCE.reponse_builder(_mot=mot, _langue=self.__langue, _horloge=self.__horloge)

    @staticmethod
    def palindrome_detection(mot: str) -> bool:
        """
        Renvoie vrai si le _mot est un palindrome, sinon faux.
        : param _mot:
        : return bool:
        """
        return mot == mot[::-1]

    @staticmethod
    def reponse_builder(_mot: str, _langue, _horloge):
        """
        Methode de construction de la reponse de l'application OHCE
        : param _mot:
        : param _langue:
        :return:
        """
        debut = OHCE.debut_builder(_langue, _horloge)
        milieu = f'{_mot[::-1]}\n{_langue.message_bien_dit}\n' if OHCE.palindrome_detection(_mot) else _mot[::-1] + '\n'
        fin = OHCE.fin_builder(_langue, _horloge)
        return debut + milieu + fin

    @saut_de_ligne
    @staticmethod
    def debut_builder(_langue, _horloge):
        return _langue.message_bonjour(_horloge)

    @saut_de_ligne
    @staticmethod
    def fin_builder(_langue, _horloge):
        return _langue.message_au_revoir(_horloge)

    # DEBUG
    def bien_dit(self):
        """
        L'application dit « Bien dit » dans la langue choisie suite à un palindrome.
        :return:
        """
        return self.__langue.message_bien_dit

    def bonjour(self):
        """
        L'application dit « Bonjour » dans la langue choisie au début du message.
        :return:
        """
        return self.__langue.message_bonjour

    def au_revoir(self):
        """
        L'application dit « Au revoir » dans la langue choisie à la fin du message.
        :return:
        """
        return self.__langue.message_au_revoir



