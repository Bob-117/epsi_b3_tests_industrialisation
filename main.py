from src import OHCE, Langue, Horloge, OhceUiCli
from test.utils import HorlogeBuilder, LangueStub


def process(_result):
    for res in _result:
        ohce = OHCE(Langue(res[0]), Horloge())
        miroir1 = ohce.palindrome(res[1])

        _horloge = HorlogeBuilder().prends_comme_heure(res[2])
        ohce = OHCE(langue=LangueStub(), horloge=_horloge)
        miroir2 = ohce.palindrome(res[1])
        yield miroir1, miroir2


if __name__ == '__main__':
    a = OhceUiCli()
    # a.jouer(langue=)
    # a = OHCE(langue=Langue('fr'), horloge=Horloge())
    # a.jouer()
    # b = a.palindrome('gibson')
    # print(b)
    # lang = ['fr', 'en']
    # words = ['gibson', 'jimi', 'radar', 'sg']
    # hours = [_ for _ in range(0, 24, 22)]
    #
    # for _ in process((_l, _w, _h) for _w in words for _l in lang for _h in hours):
    #     print(_)
