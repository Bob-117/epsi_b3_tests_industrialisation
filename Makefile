# I can use make command with Linux & Windows i guess ?
ifeq ($(OS), Windows_NT)
	python_interpreter:=python
else
	python_interpreter:=python3
endif

debug:
	@echo $(python_interpreter)
	@echo $(OS)

test_step1:
	$(python_interpreter) -m pytest -vvvv test/test_ohce_etape1.py

test_step2:
	$(python_interpreter) -m pytest -vvvv test/test_ohce_etape2.py

test_step3:
	$(python_interpreter) -m pytest -vvvv test/test_ohce_etape3.py

test_step4_defaut:
	$(python_interpreter) -m pytest -vvvv test/test_ohce_etape4_defaut.py

test_step4_recette:
	$(python_interpreter) -m pytest -vvvv test/test_ohce_etape4_recette.py

test_step4: test_step4_defaut test_step4_recette

test_evaluation:
	$(python_interpreter) -m pytest -v -s -k "test_ohce_etape"

test_all:
	$(python_interpreter) -m pytest -vvvv test/

sandbox:
	$(python_interpreter) -m pytest -vvvv test/sandbox/test_sandbox.py

car:
	$(python_interpreter) -m pytest -vvvv test/sandbox/test_car.py

