# Tests & Industrialisation

```shell
# installation
git clone
pip install -r requirements.txt
```

```shell
# execution des tests avec python
python -m pytest -v -s -k "test_ohce"

python -m pytest test/test_ohce_etape1.py
python -m pytest test/test_ohce_etape2.py
python -m pytest test/test_ohce_etape3.py
python -m pytest test/test_ohce_etape4_defaut.py
python -m pytest test/test_ohce_etape4_recette.py

# disclaimer : attention à la commande python (python, py, python3) utilisée
```

```shell
# execution des tests avec le makefile
make test_evaluation

make test_step1
make test_step2
make test_step3
make test_step4

# disclaimer : attention à l'installation de Make'
```

### Contexte

Ce projet a pour but de mettre en application la méthodologie TDD (Test Driven Development) de facon incrémentale. 
Nous développerons une bibliothèque de classes selon les exigences d'un cahier des charges, en implémentant les tests avant les méthodes.

### Cahier des charges :

- Étape 1
  - [ ] Une classe « OHCE » ayant une méthode « string Palindrome(string input) »
  - [ ] Elle renvoie en miroir ce que vous écrivez.
  - [ ] Avant de répondre, elle vous dit « bonjour ».
  - [ ] Si vous saisissez un palindrome, elle répond « Bien dit ! »
  - [ ] Après sa réponse, elle vous dit « Au revoir ».
- Étape 2
  - [ ] Implémentation de deux langues
  - [ ] Utilisation de Stub
- Étape 3
  - [ ] Langue et période de la journée
  - [ ] Utilisation de builders dans les tests.
- Étape 4
  - [ ] Reliez La langue à celle du système
  - [ ] Reliez Le moment de la journée à l’horloge
  - [ ] Reliez L’entrée et la sortie à la console
  - [ ] Réaliser un test de défaut, car le client souhaite des sauts de ligne dans la reponse de l'application
  - [ ] Réaliser des tests de recette


### Documentation technique

| Solution          | Nom     | Version                  |
|-------------------|---------|--------------------------|
| IDE               | Pycharm | Community Edition 2022.2 |
| Language          | Python  | 3.10                     |
| Framework de test | Pytest  | 7.2.1                    |

<details>
<summary>Code python</summary>

```python
# method
def my_sum(_x, _y):
    """Two numbers sum method"""
    return _x + _y

# test pattern
def test_exemple():
    # Arrange
    x, y = 1, 2
    # Act
    z = my_sum(_x = x, _y = y)
    # Assert
    assert z == 3

# expected = 1 + 2
# assert z == expected
```
<div align="center" style="font-size:.8em; margin-top: -15px; margin-bottom: 40px">Pattern en python</div>
<br><br><br>

</details>

<details>
<summary>Execution des tests et TDD</summary>

<ins>Démarche</ins> : 

A la lecture du sujet, j'ai récupéré toutes les informations "ÉTANT DONNE QUAND ALORS". J'ai écrit mes tests avec ces informations en docstring.
Initialement, j'ai créé une branche develop depuis master et j'ai mis en place une pipeline de tests avec mon repo Gitlab.

Pour chacun des étapes de l'exercice : 
  - Création d'une branch dédiée à l'étape (depuis develop)
  - Échec d'un test
  - Implementation du code
  - Succès du test
  - Je repasse tous les tests (/!\ regression)
  - Je refacto si besoin
  - Je push sur la branche dédiée
  - Si la pipeline est verte, alors je merge (manuellement) dans develop
  - Je mets à jour la documentation et nettoie le code
  - Je push et si la pipeline est verte, alors je merge (manuellement) dans master

A terme, je souhaite containeriser mon application avec Docker, build l'image si la pipeline passe, puis publier l'image.

</details>


### Modelisation 

<div style="text-align: center;">

```mermaid
stateDiagram-v2
    direction LR
    code: Code
    test1: Test
    test2: Test Loop
    state TDD {
        direction LR
        [*] --> test1
        test1 --> code: Fail
        code --> test2
        test2 --> code : Fail
        test2 --> [*]: Success
    }
```

</div>

<div align="center" style="font-size:.8em; margin-top: -15px">Méthodologie TDD</div>

<br><br><br>

<details>
<summary>Diagrammes</summary>

```mermaid
graph TB

    classDef Code fill:#337AFF; 
    classDef Test fill:#E3EE07;
    classDef Ok fill:#33FF6E;
    classDef Fail fill:#EE2A07;

    Test1["Ecrire un test"]:::Code
    Echec["Le test échoue"]:::Fail
    Succes["Le test passe"]:::Ok
    Implementation["Implémenter la methode"]:::Code
    Test2["Rejouer le test"]:::Test
    Incremental["Ecrire le test suivant"]:::Code
    
    Autres1["Les autres TRUE "]:::Ok
    Autres2["Les autres FALSE"]:::Fail
    Refacto:::Code
    
    Test1 --> Echec --> Implementation --> Test2--> Echec
    Test2 --> Succes 
    Succes --> Autres1 
    Succes --> Autres2
    Autres2 --> Refacto --> Test2
    Autres1 --> Incremental
    Incremental --> Test1
```
<div align="center" style="font-size:.8em; margin-top: -15px">Schéma récursif TDD Incrémental</div>

<br><br><br>

```mermaid
flowchart TB
%%{ init : { "theme" : "default", "flowchart" : { "curve" : "linear" }}}%%
    classDef Code fill:#337AFF; 
    classDef Test fill:#E3EE07;
    classDef Ok fill:#33FF6E;
    classDef Fail fill:#EE2A07;

    Test1["Write a test"]:::Code
        Coder["Code"]:::Code
        Echec["Fail"]:::Fail
        Test2["Test"]:::Test
    Succes["Succes"]:::Ok
    
    Test1 --> Echec --> Coder
    Coder --> Test2 --> Echec
    Test2 --> Succes --> |Next| Test1

```
<div align="center" style="font-size:.8em; margin-top: -15px">Schéma simplifié</div>

<br><br><br>

```mermaid
sequenceDiagram
    participant 1 as Début
    participant 2 as Initialisation du test
    participant 3 as Code
    participant 4 as Test en cours
    participant 5 as Test en totalité
    participant 6 as Incremental 

    1->>2: On écrit le test
    2->>3: Le test échoue
    
    loop 
        3->>4: On (ré)écrit le code
        4->>5: le test passe
        4->>3: Le test échoue
        5->>6: Les autres tests passent
        5->>3: Les autres tests ne passent plus
    end
    6->>1: On passe à la suite du cahier des charges
```
<div align="center" style="font-size:.8em; margin-top: -15px">Diagramme de séquence</div>
<br><br><br>

```mermaid
graph TB

    subgraph OhceUiCli
      Jouer["jouer_une_partie()"]
      Demander["demander_un_mot()"]
      Afficher["afficher_la_reponse()"]
    end
    subgraph Game[ ]
      Conf[ ]
      Langue["Langue()"] --> Conf
      Horloge["Horloge()"] --> Conf
      Conf --> OHCE --> Demander --> palindrome["palindrome()"] --> Afficher
    end
    Jouer -->  Conf
```
<div align="center" style="font-size:.8em; margin-top: -15px">Architecture</div>

</details>

### Itération

#### 1) L'application OHCE attend un mot en entrée nous le renvoie en miroir.

<details>
<summary>Étape 1 - Palindrome</summary>

#### Énoncé :
- QUAND on saisit une chaîne ALORS celle-ci est renvoyée en miroir.
- QUAND on saisit un palindrome ALORS celui-ci est renvoyé ET « Bien dit » est envoyé ensuite.
- QUAND on saisit une chaîne ALORS « Bonjour » est envoyé avant toute réponse.
- QUAND on saisit une chaîne ALORS « Au revoir » est envoyé en dernier.

| Arranger                                           | Agir                     | Auditer                                                                                                   |
|----------------------------------------------------|--------------------------|-----------------------------------------------------------------------------------------------------------|
| QUAND on saisit une chaîne <br> `mot = 'python'`   | `resultat = method(mot)` | ALORS celle-ci est renvoyée en miroir <br> `On veut resultat == 'nohtyp' valeur connue`                   |
| QUAND on saisit un palindrome <br> `mot = 'radar'` | `resultat = method(mot)` | ALORS celui-ci est renvoyé<br>ET « Bien dit » est envoyé ensuite<br>`On veut resultat == 'radarBien dit'` |
| QUAND on saisit une chaîne <br> `mot = 'cyber'`    | `resultat = method(mot)` | ALORS « Bonjour » est envoyé avant toute réponse<br> `On veut resultat commence par 'Bonjour'`            |
| QUAND on saisit une chaîne <br> `mot = 'cyber'`    | `resultat = method(mot)` | ALORS « Au revoir » est envoyé en dernier<br> `On veut resultat finit par 'Au revoir'`                    |
<!--
- QUAND on saisit une chaîne ALORS celle-ci est renvoyée en miroir.
- QUAND on saisit un palindrome ALORS celui-ci est renvoyé ET « Bien dit » est envoyé ensuite.
- QUAND on saisit une chaîne ALORS « Bonjour » est envoyé avant toute réponse.
- QUAND on saisit une chaîne ALORS « Au revoir » est envoyé en dernier.
-->
</details>

#### 2) L'application OHCE nous parle maintenant dans une langue choisie par l'utilisateur.

<details>
<summary>Étape 2 - Ajout d'une langue (utilisation de Stub pour corriger l'étape 1)</summary>

#### Énoncé :
- ÉTANT DONNE un utilisateur parlant une langue QUAND on entre un palindrome ALORS il est renvoyé ET le < bienDit > de cette langue est envoyé.
- ÉTANT DONNE un utilisateur parlant une langue QUAND on saisit une chaîne ALORS < bonjour > de cette langue est envoyé avant tout.
- ÉTANT DONNE un utilisateur parlant une langue QUAND on saisit une chaîne ALORS < auRevoir > dans cette langue est envoyé en dernier.

| ÉTANT DONNE                       | QUAND                                     | ALORS                                                                                                       |
|-----------------------------------|-------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| un utilisateur parlant une langue | on entre un palindrome<br>`mot = 'radar'` | 1) il est renvoyé<br>2) le < bienDit > de cette langue est envoyé.<br> `On veut Bonjourradar && Helloradar` |
| un utilisateur parlant une langue | on saisit une chaîne<br>`mot = 'python'`  | < bonjour > de cette langue est envoyé avant tout. `On veut Bonjour<reponse> && Hello<reponse>`             |
| un utilisateur parlant une langue | on saisit une chaîne<br>`mot = 'python'`  | < auRevoir > dans cette langue est envoyé en dernier. `On veut <reponse>auRevoir && <reponse>goodBye`       |

</details>

#### 3) L'application OHCE adapte ses réponses à la période de la journée.

<details>
<summary>Étape 3 - Ajout de l'heure (utilisation de builders qui permettent d’ajouter des cas de manière indolore.)</summary>

#### Énoncé :
- ÉTANT DONNE un utilisateur parlant une langue ET que la période de la journée est < période > QUAND on saisit une chaîne ALORS < salutation > de cette langue à cette période est envoyé avant tout

```json
  { 
    "cas_step_31": {
      "matin": "bonjour_am",
      "après-midi": "bonjour_pm",
      "soirée": "bonjour_soir",
      "nuit": "bonjour_nuit"
    }
  }

```
- ETANT DONNE un utilisateur parlant une langue ET que la période de la journée est < période > QUAND on saisit une chaîne  ALORS < auRevoir > dans cette langue à cette période est envoyé en dernier
```json
  { 
    "cas_step_32": {
      "matin": "auRevoir_am",
      "après-midi": "auRevoir",
      "soirée": "auRevoir",
      "nuit": "auRevoir_nuit"
    }
  }
```
| ÉTANT DONNE                                                                            | QUAND                                    | ALORS                                                                                 |
|----------------------------------------------------------------------------------------|------------------------------------------|---------------------------------------------------------------------------------------|
| 1) un utilisateur parlant une langue<br>2)que la période de la journée est < période > | on saisit une chaîne<br>`mot = 'python'` | < salutation > de cette langue à cette période est envoyé avant tout<br>`On veut ...` |
| 1) un utilisateur parlant une langue<br>2)que la période de la journée est < période > | on saisit une chaîne<br>`mot = 'python'` | < auRevoir > dans cette langue à cette période est envoyé en dernier<br>`On veut ...` |

</details>


#### 4 - a) Amelioration de l'application, integration

<details>
<summary>Étape 4a) L'application OHCE est reliée à la langue et à l'heure du système, ainsi qu'à une entrée/sortie console.</summary>

- [X] Heure implémentée dans l'étape 3 (...)
- [X] Langue du système (...)
- [X] Entrée/Sortie cli

</details>

#### 4 - b) Retour client

<details>
<summary> Étape 4b) Test de défaut et test de recette</summary>

- [X] Test de défaut : sauts de ligne

Suite à la livraison du code, le client s'aperçoit que l'application OHCE envoie des réponses peu, voire non formattées et souhaite l'ajout de saut de ligne entre les différentes composantes du message de retour.
Utilisation d'un decorator *@saut_de_ligne* dans `src/utils.py`, si le client souhaite deux sauts de ligne la semaine prochaine, une seule occurrence à changer.
- [X] Test de recette

Afin d'effectuer une démonstration de l'application, nous rédigerons 3 tests de recettes selon les cas suivants :
  - scénario 1 : Automatique, palindrome, anglais, soir
  - scénario 2 : Automatique, non-palindrome, français, matin.
  - scénario 3 : Saisie libre du client, langue et moment actuels du système

Les tests de recette sont un support de scénario lors d'une démonstration client. Certains sont purement automatiques, d'autres font intervenir le client.

</details>

### Sandbox

Le module `test/sandbox` regroupe des exemples des différentes utilisations de pytest.

 - [X] Parametrize
 - [X] Mark
 - [X] XFail
 - [X] Enregistrement dans un fichier
 - [X] Stub
 - [X] Spy
 - [X] Dummy
 - [X] Builder
 - [X] Generator

Le fichier `test/sandbox/test_sandbox.py` montre une implémentation de tests pytest avec enregistrement des résultats dans un fichier.

L'ensemble de fichiers `test/sandbox/test_car.py`, `test/sandbox/car.py` & `test/sandbox/car_generator.py` présente les features basiques de pytest
ainsi que beaucoup d'outils et astuces utiles dans la rédaction d'un jeu de tests pertinent.

### TODO 

 - [X] refacto graph
 - [X] requirements pytest
 - [X] step 1 
 - [X] step 2 
 - [X] step 3 
 - [X] step 4
 - [X] makefile
 - [X] gitlab-ci
 - [ ] choix de langue https://gitlab.com/Bob-117/Casino/-/blob/master/src/utils/load_lang.py
 - [X] cheat sheet pytest (voir `test/sandbox`)
 - [ ] Pytest options (-v -s -k, -vvvv...)
 - [ ] Stub (pas que) avec monkeypatch

### License
Open Source 100%